//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Data.Services.Providers;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;

namespace AdvntureWorksService
{
    //public class AdventureWorksService : DataService< /* TODO: put your data source class name here */ >
    //public class AdventureWorksService : DataService<AdventureWorks2014Entities>
    public class AdventureWorksService : EntityFrameworkDataService<AdventureWorks2014Entities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
            config.SetEntitySetAccessRule("SalesOrderHeaders", EntitySetRights.All);
            config.UseVerboseErrors = true;
        }

        // This method is called only once to initialize service-wide policies.
        //public static void InitializeService(IDataServiceConfiguration config)
        //{
        //    config.SetEntitySetAccessRule("SalesOrderHeaders", EntitySetRights.All);
        //}
    }
}
