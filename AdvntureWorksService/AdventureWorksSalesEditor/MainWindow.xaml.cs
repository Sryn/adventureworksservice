﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AdventureWorksSalesEditor.ServiceReference1;

namespace AdventureWorksSalesEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private AdventureWorks2014Entities dataServiceClient;
        private System.Data.Services.Client.DataServiceQuery<SalesOrderHeader> salesQuery;
        private CollectionViewSource ordersViewSource;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // TODO: Modify the port number in the following URI as required.
            dataServiceClient = new AdventureWorks2014Entities(new Uri("http://localhost:49666/AdventureWorksService.svc"));
            salesQuery = dataServiceClient.SalesOrderHeaders;

            ordersViewSource = ((CollectionViewSource)(this.FindResource("salesOrderHeadersViewSource")));
            ordersViewSource.Source = salesQuery.Execute();
            ordersViewSource.View.MoveCurrentToFirst();

            //System.Windows.Data.CollectionViewSource salesOrderHeadersViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("salesOrderHeadersViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // salesOrderHeadersViewSource.Source = [generic data source]
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (ordersViewSource.View.CurrentPosition > 0)
                ordersViewSource.View.MoveCurrentToPrevious();
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if(ordersViewSource.View.CurrentPosition < ((CollectionView)ordersViewSource.View).Count - 1)
            {
                ordersViewSource.View.MoveCurrentToNext();
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SalesOrderHeader currentOrder = (SalesOrderHeader)ordersViewSource.View.CurrentItem;
            dataServiceClient.UpdateObject(currentOrder);
            dataServiceClient.SaveChanges();
        }
    }
}
