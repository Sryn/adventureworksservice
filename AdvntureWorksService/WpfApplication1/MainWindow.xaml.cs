﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Uri svcUri = new Uri("http://localhost:49666/AdventureWorksService.svc/");

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AdventureWorksServiceReference.AdventureWorks2014Entities ctx = new AdventureWorksServiceReference.AdventureWorks2014Entities(svcUri);

            System.Windows.Data.CollectionViewSource salesOrderHeadersViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("salesOrderHeadersViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // salesOrderHeadersViewSource.Source = [generic data source]
            salesOrderHeadersViewSource.Source = ctx.SalesOrderHeaders.ToList();
        }
    }
}
